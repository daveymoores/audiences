var keystone = require('keystone');

exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
	locals.data = {};


	view.on('init', function(next) {

		keystone
			.list('Contacts')
			.model
			.find()
			.exec(function(err, result) {
				locals.data.contacts = result;
				next(err);
			});

	});

	view.on('init', function(next) {

        keystone
            .list('Articles')
            .model
			.find()
            .exec(function(err, result) {
                locals.data.articles = result;
                next(err);
            });

    });

    view.on('init', function(next) {

        keystone
            .list('Segment')
            .model
			.findOne({
                key: req.params.segment
            })
            .exec(function(err, result) {
                locals.data.segments = result;
                next(err);
            });

    });

	view.on('init', function(next) {

        keystone
            .list('SubCategory')
            .model
			.findOne()
			.where('Segments', locals.data.segments.id)
            .exec(function(err, result) {
                locals.data.subcategory = result;
                next(err);
            });

    });

	view.on('init', function(next) {

        keystone
            .list('Category')
            .model
			.findOne()
			.where('SubCategories', locals.data.subcategory.id)
            .exec(function(err, result) {
                locals.data.category = result;
                next(err);
            });

    });


	// Render the view
	view.render('search');
};
