class CopySegment {
    constructor(node){
        this.node = node;
        this.init();
		this.attachSelectors();
    }

    init(){
		this._copiedText = document.getElementsByClassName('zd-table--copy');
    }

	attachSelectors(){

		[].forEach.call(this._copiedText, (element, index, array)=>{
			element.addEventListener('click', (e)=>{
				e.preventDefault();
				var parent = e.currentTarget.parentNode;
				var segment = parent.getAttribute('data-label');
				this.copyToClipboard(segment);
				this.removeCopied();
				element.innerText = 'Copied';
			});
		});
	}

	removeCopied(){
		[].forEach.call(this._copiedText, (element, index, array)=>{
			element.innerText = 'Copy to clipboard';
		});
	}

	copyToClipboard(text) {
	    if (window.clipboardData && window.clipboardData.setData) {
	        // IE specific code path to prevent textarea being shown while dialog is visible.
	        return clipboardData.setData("Text", text);

	    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
	        var textarea = document.createElement("textarea");
	        textarea.textContent = text;
	        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
	        document.body.appendChild(textarea);
	        textarea.select();
	        try {
	            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
	        } catch (ex) {
	            console.warn("Copy to clipboard failed.", ex);
	            return false;
	        } finally {
	            document.body.removeChild(textarea);
	        }
	    }
	}
}

module.exports = CopySegment;
