const axios = require("axios");
const Fuse = require("fuse.js");

class SearchInput {
	constructor(node) {
		this.node = node;
		this.input = this.node.querySelector("#segment-search");
		this.results = document.getElementById("zd-form-autocomplete-js");
		this.cross = document.getElementById("zd-form-cross-js");

		this.fetchAllData();
	}

	createListItem(content, slug) {
		const li = document.createElement("li");
		const a = document.createElement("a");
		li.classList.add("zd-form__autocomplete--result");
		a.setAttribute("href", slug);
		a.innerText = content;
		li.appendChild(a);
		this.results.appendChild(li);
	}

	clearSearch() {
		this.results.classList.remove("active");
		this.cross.classList.remove("active");
		this.results.innerHTML = "";
		this.input.value = "";
	}

	searchOnKeyup(e) {
		const dataArray = [];
		const showResults = this.debounce(str => {
			var value = str.trim();
			this.results.classList.add("active");
			this.cross.classList.add("active");

			if (value === "" || value.length <= 0) {
				//handle situation when there is nothing in the search
				this.results.classList.remove("active");
				this.cross.classList.remove("active");
			}

			this.results.innerHTML = "";

			const results = this.fuse.search(value);

			if (results.length === 0) {
				this.createListItem("No results found", "#_");
			} else {
				results.map((result, index) => {
					if (index <= 10) {
						this.createListItem(result.SegmentName, `segment/${result.key}`);
					}
				});
			}
		}, 200);

		showResults(e.currentTarget.value);
	}

	fetchAllData() {
		let data = null;
		axios
			.post("/search")
			.then(response => {
				// handle success
				this.setupSearch(response.data);
			})
			.catch(error => {
				// handle error
				console.log(error);
			});
	}

	setupSearch(data) {
		var options = {
			shouldSort: true,
			tokenize: true,
			matchAllTokens: true,
			threshold: 0.6,
			location: 0,
			distance: 100,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			keys: ["SegmentLabel", "key"]
		};

		this.fuse = new Fuse(data, options);
		this.input.addEventListener("keyup", this.searchOnKeyup.bind(this));
		this.cross.addEventListener("click", this.clearSearch.bind(this));
	}

	debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this,
				args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}
}

module.exports = SearchInput;
