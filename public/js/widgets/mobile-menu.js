class MobileMenu {
    constructor(node){
		this.node = node;
		this.init();
    }

    init(){
		this.btn = this.node.querySelector('.zd-close-button');
		this.burger = document.getElementById('nav-icon');
        this.background = document.getElementById('zd-bg-js');

		this.btn.addEventListener('click', (e)=>{
			this.node.classList.toggle('open');
			this.burger.classList.toggle('open');
            this.background.classList.toggle('open');

            if(this.node.classList.contains('open')) {
                this.background.style.zIndex = 198;
            } else {
                setTimeout(()=>{
                    this.background.style.zIndex = -99;
                }, 300)
            }
		});
    }
}

module.exports = MobileMenu;
