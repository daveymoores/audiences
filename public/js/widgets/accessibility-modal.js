class AccessibilityModal {
	constructor (node) {
		this.node = node;
		this.modal = document.getElementById('modal');
		this.bg = document.getElementById('bg');
		this.close = this.modal.querySelector('.close');

		this.init();
	}

	init () {
		var ctx = this;

		this.node.addEventListener('click', function (e) {
			e.preventDefault();
			ctx.modal.classList.add('active');
			ctx.bg.classList.add('active');
		});

		this.close.addEventListener('click', function (e) {
			e.preventDefault();
			ctx.modal.classList.remove('active');
			ctx.bg.classList.remove('active');
		});
	}
}

module.exports = AccessibilityModal;
