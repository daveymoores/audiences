(() => {
	"use strict";

	let widgets;
	const $body = document.body;

	widgets = {
		"copy-segment": require("./widgets/copy-segment"),
		"mobile-menu": require("./widgets/mobile-menu"),
		"search-input": require("./widgets/search-input"),
		"accessibility-modal": require("./widgets/accessibility-modal")
	};

	function initWidgets($node) {
		let dw = $node.querySelectorAll("[data-widget]");
		Array.prototype.forEach.call(dw, (el, index, array) => {
			let type;
			type = el.getAttribute("data-widget");
			console.log(type);

			if (widgets[type]) {
				new widgets[type](el);
			}
		});
	}

	initWidgets($body);
})();
