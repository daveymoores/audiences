var gulp = require("gulp");
var watch = require("gulp-watch");
var shell = require("gulp-shell");
var sourcemaps = require("gulp-sourcemaps");

var browserify = require("browserify");
var babelify = require("babelify");
var source = require("vinyl-source-stream");
var gutil = require("gulp-util");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var postcss = require("gulp-postcss");

var paths = {
	src: ["./models/**/*.js", "./routes/**/*.js", "keystone.js", "package.json"],
	style: {
		all: "./public/styles/**/*.scss",
		output: "./public/styles/"
	}
};

var supported = [
	"last 2 versions",
	"safari >= 8",
	"ie >= 10",
	"ff >= 20",
	"ios 6",
	"android 4"
];

gulp.task("watch:sass", function() {
	gulp.watch(paths.style.all, ["sass"]);
});

// task
gulp.task("es6", function() {
	browserify({ debug: true })
		.transform(babelify)
		.require("./public/js/main.js", { entry: true })
		.bundle()
		.on("error", gutil.log)
		.pipe(source("main.min.js"))
		.pipe(gulp.dest("./public/js/"));
});

gulp.task("watch:es6", function() {
	gulp.watch(["./public/js/**/*.js"], ["es6"]);
});

gulp.task("sass", function() {
	gulp
		.src(paths.style.all)
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer(supported))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.style.output));
});

gulp.task("runKeystone", shell.task("node keystone.js"));
gulp.task("watch", ["watch:es6", "watch:sass"]);

gulp.task("default", ["watch", "runKeystone"]);
