var keystone = require('keystone');
var Types = keystone.Field.Types;
var Articles = new keystone.List('Articles', {
    sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'ArticleName' },
    autokey: { path: 'key', from: 'ArticleName', unique: true }
});

Articles.add({
    ArticleName: { type: String, index: true },
	ArticleUrl: { type: Types.Url },
	Website: { type: String, index: true },
	WebsiteUrl: { type: Types.Url }
});

Articles.register();
