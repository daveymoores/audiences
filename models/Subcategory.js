var keystone = require('keystone');
var Types = keystone.Field.Types;
var SubCategory = new keystone.List('SubCategory', {
    sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'SubCategoryName' },
    autokey: { path: 'key', from: 'SubCategoryName', unique: true }
});

SubCategory.add({
    SubCategoryName: { type: String, index: true },
	SubCategoryImage: { type: Types.S3File, filename: function(item, filename, originalname){
        return originalname;
    }},
	Segments: { type: Types.Relationship, ref: 'Segment', many: true }
});

SubCategory.relationship({ ref: 'Category', path: 'subcategories', refPath: 'SubCategories'});

SubCategory.register();
