var keystone = require('keystone');
var Types = keystone.Field.Types;
var Contacts = new keystone.List('Contacts', {
    sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'Name' },
    autokey: { path: 'key', from: 'Name', unique: true }
});

Contacts.add({
    Name: { type: String, index: true },
	Position: { type: String },
	ContactNumber: { type: String, index: true },
	Email: { type: Types.Email, index: true }
});

Contacts.register();
