var mongoosastic = require('mongoosastic');
var elasticsearch = require('elasticsearch');
var keystone = require('keystone');

var Types = keystone.Field.Types;
var Segment = new keystone.List('Segment', {
    sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'SegmentName' },
    autokey: { path: 'key', from: 'SegmentName', unique: true }
});

Segment.add({
    SegmentName: { type: String, initial: true, index: true },
	SegmentLabel: { type: String, initial: true, index: true },
});

Segment.relationship({ ref: 'SubCategory', path: 'Segments', refPath: 'Segments'});

// var esClient = new elasticsearch.Client({
//   host: 'https://search-segment-data-7l2qrszg3ruftfpvzlquifgag4.us-east-1.es.amazonaws.com', //'localhost:9200',//process.env.BONSAI_URL, //protocol: "https",
//   log: 'trace'
// });

// Segment.schema.plugin(mongoosastic, {
//   esClient: esClient
// });

// esClient.ping({
//   requestTimeout: 30000,
// }, function (error) {
//   if (error) {
//     console.error('elasticsearch cluster is down!');
//   } else {
//     console.log('All is well');
//   }
// });


Segment.register();
