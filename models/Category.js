var keystone = require('keystone');
var Types = keystone.Field.Types;
var Category = new keystone.List('Category', {
    sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'CategoryName' },
    autokey: { path: 'key', from: 'CategoryName', unique: true }
});

Category.add({
    CategoryName: { type: String, index: true },
	CategoryImage: { type: Types.S3File, filename: function(item, filename, originalname){
        return originalname;
    }},
	SubCategories: { type: Types.Relationship, ref: 'SubCategory', many: true }
});

Category.register();
